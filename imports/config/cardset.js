//Settings for new cardsets
let defaultSortTopicContentByDateCreate = true;

let kindsOwnedByServer = ['demo', 'makingOf', 'server'];

module.exports = {
	defaultSortTopicContentByDateCreate,
	kindsOwnedByServer
};
